package thsst.ontopop.ontology_population;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.ArrayList;

public class InstanceExtraction {

    private static ArrayList<String> Condition = new ArrayList<String>();
    private static ArrayList<String> Symptom = new ArrayList<String>();
    private static ArrayList<String> Deficient = new ArrayList<String>();
    private static ArrayList<String> Excess = new ArrayList<String>();
    private static ArrayList<String> Need = new ArrayList<String>();
    private static ArrayList<String> Avoid = new ArrayList<String>();
    private static boolean isSameName = false;

    public InstanceExtraction() throws Exception  {

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setValidating(false);
        DocumentBuilder db;

        try {
            db = dbf.newDocumentBuilder();
            ArrayList<String> attri = new ArrayList<String>();
            Document doc = db.parse(new FileInputStream(new File("SampleXML - Copy.xml")));
            NodeList nodeList = doc.getElementsByTagName("Entity");
            System.out.println("Number of entities found: " + nodeList.getLength() + "\n");




            for (int i = 0; i < nodeList.getLength(); i++) {
                Element node = (Element) nodeList.item(i);
                listAllAttributes(node, attri, nodeList, i);
            }
            AppendToFile1();
            //CreateOntology();
        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void AppendToFile1() throws IOException {

        try {
            File ontologyFile = new File("ontology.ttl");
            FileWriter fw = new FileWriter(ontologyFile);

            if (!ontologyFile.exists()) {
                CreateOntology();
            }

            OntModel model = ModelFactory.createOntologyModel();
            model.read(new FileInputStream(ontologyFile), null, "TTL");

            Resource resource = model.getResource("Entity Type: Condition/" + Condition.get(0));
            System.out.println("Resource: " + resource.toString().toString());
            System.out.println("ArrayList: " + Condition.get(0));

            // Name
            Property name = model.getProperty("file:///C:/Users/DanielleLladoc/Workspace/OntologyPopulationModule/Name");
            StmtIterator iter = resource.listProperties(name);
            while(iter.hasNext()){
                String nametemp = iter.nextStatement().getObject().toString();
                if(nametemp.equals(Condition.get(0))){
                    isSameName = true;
                }
                System.out.println("Name: " + nametemp);
            }

            if(isSameName == true){
                // hasSymptom
                Property symptom = model.getProperty("file:///C:/Users/DanielleLladoc/Workspace/OntologyPopulationModule/hasSymptom");
                StmtIterator iter1 = resource.listProperties(symptom);
                while(iter1.hasNext()){
                    String symptemp = iter1.nextStatement().getObject().toString();
                    for(int i = 0; i < Symptom.size(); i++){
                        if(symptemp.equals(Symptom.get(i))){
                            Symptom.remove(i);
                        }
                    }

                    System.out.println("Symptoms: " + symptemp);
                }

                // hasDeficiency
                Property deficient = model.getProperty("file:///C:/Users/DanielleLladoc/Workspace/OntologyPopulationModule/hasDeficiency");
                StmtIterator iter2 = resource.listProperties(deficient);
                while(iter2.hasNext()){
                    String deftemp = iter2.nextStatement().getObject().toString();
                    for(int i = 0; i < Deficient.size(); i++){
                        if(deftemp.equals(Deficient.get(i))){
                            Deficient.remove(i);
                        }
                    }
                    System.out.println("Nutrient Deficient: " +deftemp);
                }

                // hasExcess
                Property excess = model.getProperty("file:///C:/Users/DanielleLladoc/Workspace/OntologyPopulationModule/hasExcess");
                StmtIterator iter3 = resource.listProperties(excess);
                while(iter3.hasNext()){
                    String excesstemp = iter3.nextStatement().getObject().toString();
                    for(int i = 0; i < Excess.size(); i++){
                        if(excesstemp.equals(Excess.get(i))){
                            Excess.remove(i);
                        }
                    }
                    System.out.println("Nutrient Excess: " +excesstemp);
                }

                // needs
                Property needs = model.getProperty("file:///C:/Users/DanielleLladoc/Workspace/OntologyPopulationModule/needs");
                StmtIterator iter4 = resource.listProperties(needs);
                while(iter4.hasNext()){
                    String needtemp = iter4.nextStatement().getObject().toString();
                    for(int i = 0; i < Need.size(); i++){
                        if(needtemp.equals(Need.get(i))){
                            Need.remove(i);
                        }
                    }
                    System.out.println("Food Needed: " +needtemp);
                }

                // avoids
                Property avoids = model.getProperty("file:///C:/Users/DanielleLladoc/Workspace/OntologyPopulationModule/avoids");
                StmtIterator iter5 = resource.listProperties(avoids);
                while(iter5.hasNext()){
                    String avoidtemp = iter5.nextStatement().getObject().toString();
                    for(int i = 0; i < Avoid.size(); i++){
                        if(avoidtemp.equals(Avoid.get(i))){
                            Avoid.remove(i);
                        }
                    }
                    System.out.println("Food to avoid: " +avoidtemp);
                }

                for(int i =0; i < Symptom.size(); i++){
                    System.out.println(Symptom.get(i));
                    resource.addProperty(symptom, Symptom.get(i).toString());
                }
                for(int i =0; i < Deficient.size(); i++){
                    System.out.println(Deficient.get(i));
                    resource.addProperty(deficient, Deficient.get(i).toString());
                }
                for(int i =0; i < Excess.size(); i++){
                    System.out.println(Excess.get(i));
                    resource.addProperty(excess, Excess.get(i).toString());
                }
                for(int i =0; i < Need.size(); i++){
                    System.out.println(Need.get(i));
                    resource.addProperty(needs, Need.get(i).toString());
                }
                for(int i =0; i < Avoid.size(); i++){
                    System.out.println(Avoid.get(i));
                    resource.addProperty(avoids, Avoid.get(i).toString());
                }
                model.write(fw, "Turtle").toString();
            }
            else{
                AddToOntology(fw, model);
            }


            model.write(System.out, "Turtle").toString();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }
	
	/*public static void remove(String resource, String property, String value){
		
		
		try {
			OntModel model = ModelFactory.createOntologyModel();
			model.read(new FileInputStream("C:/Users/DanielleLladoc/Desktop/ontology.ttl"), null, "TTL");
			
			Resource resource1 = model.getResource("Entity Type: Condition/"+Condition.get(0));
		    
		
		
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/

    public static void listAllAttributes(Element element, ArrayList<String> attri, NodeList nodeList, int i) {

        NamedNodeMap attributes = element.getAttributes(); // get a map containing the attributes of this node
        int numAttrs = attributes.getLength(); // get the number of nodes in this map
        System.out.println("Node Name: " + element.getNodeName());

        for (int x = 0; x < numAttrs; x ++) {
            Attr attr = (Attr) attributes.item(x);
            String attrName = attr.getNodeName();
            String attrValue = attr.getNodeValue();
            attri.add(attrValue);
            System.out.println(attrName + ": " + attrValue);

            if(attrValue.equals("Condition")){
                Condition.add(nodeList.item(i).getFirstChild().getNodeValue().toString());
            }
            else if(attrValue.equals("Symptom")){
                Symptom.add(nodeList.item(i).getFirstChild().getNodeValue().toString());
            }
            else if(attrValue.equals("Deficient")){
                Deficient.add(nodeList.item(i).getFirstChild().getNodeValue().toString());
            }
            else if(attrValue.equals("Excess")){
                Excess.add(nodeList.item(i).getFirstChild().getNodeValue().toString());
            }
            else if(attrValue.equals("Need")){
                Need.add(nodeList.item(i).getFirstChild().getNodeValue().toString());
            }
            else if(attrValue.equals("Avoid")){
                Avoid.add(nodeList.item(i).getFirstChild().getNodeValue().toString());
            }
        }

        System.out.println(">> " + nodeList.item(i).getFirstChild().getNodeValue() + "\n\n");
    }

    public static void CreateOntology() {

        try {
            File file = new File("ontology.ttl");
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            fw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("\n\nSuccessfully created ontology file!");
    }

    public static void AddToOntology(FileWriter fw, OntModel model) {
        //OntModel model = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);
        String NS = "Entity Type: ";
        Resource resourceCondition = null;

        if(isSameName == true){
            resourceCondition = model.getResource("Entity Type: Condition/"+Condition.get(0));
        }
        else{
            resourceCondition = model.createResource("Entity Type: Condition/"+Condition.get(0));
        }


        Property name = model.createProperty("Name");
        resourceCondition.addProperty(name, Condition.get(0).toString());

        Property hasSymptom = model.createProperty("hasSymptom");
        for(int i = 1; i <= Symptom.size(); i++){
            resourceCondition.addProperty(hasSymptom, Symptom.get(i-1).toString());
        }

        Property hasDeficiency = model.createProperty("hasDeficiency");
        for(int i = 1; i <= Deficient.size(); i++){
            resourceCondition.addProperty(hasDeficiency, Deficient.get(i-1).toString());
        }

        Property hasExcess = model.createProperty("hasExcess");
        for(int i = 1; i <= Excess.size(); i++){
            resourceCondition.addProperty(hasExcess, Excess.get(i-1).toString());
        }

        Property needs = model.createProperty("needs");
        for(int i = 1; i <= Need.size(); i++){
            resourceCondition.addProperty(needs, Need.get(i-1).toString());
        }

        Property avoids = model.createProperty("avoids");
        for(int i = 1; i <= Avoid.size(); i++){
            resourceCondition.addProperty(avoids, Avoid.get(i-1).toString());
        }

        model.write(fw, "Turtle").toString();
    }

}