package thsst.ontopop.retrieval;

import de.l3s.boilerpipe.extractors.DefaultExtractor;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileReader;

public class Cleaner {

    private String storagePath;

    public Cleaner(String storagePath) {
        this.storagePath = storagePath;
    }

    public void clean(File file) {
        try {
            String filePath = file.getAbsolutePath();
            filePath = filePath.replaceAll("\\\\", "/");

            String filename = file.getName();
            filename = filename.replace(".html", ".txt");

            FileReader reader = new FileReader(filePath);
            FileUtils.write(new File(this.storagePath + "/clean/" + filename),
                    DefaultExtractor.INSTANCE.getText(reader));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
