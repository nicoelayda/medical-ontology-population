package thsst.ontopop.retrieval.controller.action;

import thsst.ontopop.retrieval.controller.ArticleRetrievalController;
import thsst.ontopop.retrieval.model.LogEntry;
import thsst.ontopop.retrieval.view.ArticleRetrievalView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddUrlActionListener implements ActionListener{

    private ArticleRetrievalController controller;
    private ArticleRetrievalView view;

    public AddUrlActionListener(ArticleRetrievalController controller) {
        this.controller = controller;
        this.view = controller.getView();
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        String url = view.getUrlText();

        controller.getCrawlerSeedListModel().addElement(url);
        view.setUrlText(new String());

        LogEntry entry = new LogEntry("Added " + url + " to crawler seeds.");
        view.addLogEntry(entry);

    }
}
