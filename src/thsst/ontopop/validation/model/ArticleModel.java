package thsst.ontopop.validation.model;

import org.w3c.dom.Document;

public class ArticleModel extends ConditionModel{
	private Document srcDocument;
	private String srcPath;
	
	public ArticleModel(Document src, String path){
		srcDocument = src;
		srcPath = path;
	}

	public Document getSrcDocument() {
		return srcDocument;
	}

	public void setSrcDocument(Document srcDocument) {
		this.srcDocument = srcDocument;
	}
	
	@Override
	public String toString(){
		String name = srcPath.substring(srcPath.lastIndexOf("\\")+1);;
		
		return name;
	}
}
