package thsst.ontopop.validation.model;

import java.util.ArrayList;

public class ConditionModel {
	private String conditionName;
	private ArrayList<String> synonyms;
	private ArrayList<String> symptoms;
	private ArrayList<String> neededFood;
	private ArrayList<String> avoidFood;
	private ArrayList<String> neededNutrients;
	private ArrayList<String> avoidNutrients;
	
	public String getConditionName() {
		return conditionName;
	}
	
	public void setConditionName(String conditionName) {
		this.conditionName = conditionName;
	}

	public ArrayList<String> getSynonyms() {
		return synonyms;
	}

	public void setSynonyms(ArrayList<String> otherNames) {
		this.synonyms = otherNames;
	}

	public ArrayList<String> getSymptoms() {
		return symptoms;
	}

	public void setSymptoms(ArrayList<String> symptoms) {
		this.symptoms = symptoms;
	}

	public ArrayList<String> getNeededFood() {
		return neededFood;
	}

	public void setNeededFood(ArrayList<String> neededFood) {
		this.neededFood = neededFood;
	}

	public ArrayList<String> getAvoidFood() {
		return avoidFood;
	}

	public void setAvoidFood(ArrayList<String> avoidFood) {
		this.avoidFood = avoidFood;
	}

	public ArrayList<String> getNeededNutrients() {
		return neededNutrients;
	}

	public void setNeededNutrients(ArrayList<String> neededNutrients) {
		this.neededNutrients = neededNutrients;
	}

	public ArrayList<String> getAvoidNutrients() {
		return avoidNutrients;
	}

	public void setAvoidNutrients(ArrayList<String> avoidNutrients) {
		this.avoidNutrients = avoidNutrients;
	}
}
