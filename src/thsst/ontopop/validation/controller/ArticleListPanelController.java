package thsst.ontopop.validation.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

import org.w3c.dom.Document;

import thsst.ontopop.ontology_population.OntologyPopulation;
import thsst.ontopop.validation.XMLParser;
import thsst.ontopop.validation.XMLReader;
import thsst.ontopop.validation.model.ArticleModel;
import thsst.ontopop.validation.model.ConditionModel;
import thsst.ontopop.validation.view.ArticleDetailsPanel;
import thsst.ontopop.validation.view.OntologyDetailsPanel;

public class ArticleListPanelController {
	private XMLReader reader;
	private XMLParser parser;
	private OntologyPopulation op;
	
	public ArticleListPanelController(){
		reader = new XMLReader();
		parser = new XMLParser();
		
		op = new OntologyPopulation();
		op.getAllConditions();
	}
	
	public ArrayList<ArticleModel> getArticles(String srcFolder){
		ArrayList<ArticleModel> list = new ArrayList<ArticleModel>();
		ArrayList<Document> files = reader.loadXMLFiles(new File(srcFolder));
		ArrayList<String> paths = reader.getSrcFiles();
		
		for(int i=0; i<files.size(); i++){
			Document doc = files.get(i);
			list.add(parser.parseXML(doc, paths.get(i)));
		}
		
		System.out.println(list.size());
		return list;
	}
	
	public DefaultTableModel generateTableModel(ArrayList<ArticleModel> list){
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		Vector<String> columns = new Vector<String>(Arrays.asList(new String[]{""}));
	    
		
	    Vector<Object> vector;
	    for(ArticleModel article: list) {
	        vector = new Vector<Object>();
	       
	        vector.add(article);
	     
	        data.add(vector);
	    }

	    return new DefaultTableModel(data, columns){
			public boolean isCellEditable(int row, int column){
				return false;
			}
		};
	}
	
	public void loadArticleDetails(ArticleModel article){
		OntologyDetailsPanel.getInstance().resetDetailsPanel();
		ArticleDetailsPanel.getInstance().resetDetailsPanel();
		
		if(article!= null){
			ArticleDetailsPanel.getInstance().loadArticleDetails(article);
			
			ConditionModel cm = op.getCondition(article.getConditionName());
			
			if(cm!=null){
				ArticleModel oCond = new ArticleModel(null, "");
				oCond.setAvoidFood(cm.getAvoidFood());
				oCond.setAvoidNutrients(cm.getAvoidNutrients());
				oCond.setConditionName(cm.getConditionName());
				oCond.setNeededFood(cm.getNeededFood());
				oCond.setNeededNutrients(cm.getNeededNutrients());
				oCond.setSymptoms(cm.getSymptoms());
				oCond.setSynonyms(cm.getSynonyms());
				
				OntologyDetailsPanel.getInstance().loadArticleDetails(oCond);
			}
		}
	}
	
	public void addTonOntology(ArticleModel article){
		try {
			op.AddToOntology(article);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
