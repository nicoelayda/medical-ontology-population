package thsst.ontopop.validation;

import java.util.ArrayList;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import thsst.ontopop.validation.model.ArticleModel;

public class XMLParser {
	/*public ArticleModel parseXML(Document doc, String path){
		ArticleModel article = new ArticleModel(doc, path);
		
		NodeList nodeList = doc.getElementsByTagName("condition");
		Node nNode = nodeList.item(0);
		if(nNode.getNodeType() == Node.ELEMENT_NODE){
			Element e = (Element)nNode;
			article.setConditionName(e.getTextContent());
		}
			
		
		article.setSynonyms(getNodeValues(doc, "synonym"));
		article.setSymptoms(getNodeValues(doc, "symptom"));
		article.setNeededFood(getNodeValues(doc, "needFood"));
		article.setAvoidFood(getNodeValues(doc, "avoidFood"));
		article.setNeededNutrients(getNodeValues(doc, "needNutrient"));
		article.setAvoidNutrients(getNodeValues(doc, "avoidNutrient"));
		
		return article;
	}
	
	private ArrayList<String> getNodeValues(Document doc, String tagName){
		ArrayList<String> values = new ArrayList<String>();
		
		NodeList nodeList = doc.getElementsByTagName(tagName);
		
		NodeList valueList = nodeList.item(0).getChildNodes();
		
		System.out.println(valueList.getLength());
		
		for(int i=0; i<valueList.getLength(); i++){
			Node nNode = valueList.item(i);
			if(nNode.getNodeType() == Node.ELEMENT_NODE){
				Element e = (Element)nNode;
				values.add(e.getTextContent());
			}
		}

		return values;
	}*/
	
	public ArticleModel parseXML(Document doc, String path){
		ArticleModel article = new ArticleModel(doc, path);
		
		NodeList nodeList = doc.getElementsByTagName("Entity");
		
		ArrayList<String> cond = new ArrayList<String>();
		ArrayList<String> synonym = new ArrayList<String>();
		ArrayList<String> symptom = new ArrayList<String>();
		ArrayList<String> nutDef = new ArrayList<String>();
		ArrayList<String> nutEx = new ArrayList<String>();
		ArrayList<String> foodAv = new ArrayList<String>();
		ArrayList<String> foodNe = new ArrayList<String>();
			
		for(int i=0; i<nodeList.getLength(); i++){
			Element node = (Element) nodeList.item(i);
			NamedNodeMap attributes = node.getAttributes();
			int numAttrs = attributes.getLength();
			ArrayList<String> attri = new ArrayList<String>();
			
			for(int y=0; y<numAttrs; y++){
				Attr attr = (Attr) attributes.item(y);
				String attrName = attr.getNodeName();
				String attrValue = attr.getNodeValue();
				attri.add(attrValue);
				System.out.println(attrName + ": " + attrValue);
				
				if(attrValue.equals("Condition")){
					cond.add(nodeList.item(i).getFirstChild().getNodeValue().toString());
				}
				else if(attrValue.equals("Synonym")){
					synonym.add(nodeList.item(i).getFirstChild().getNodeValue().toString());
				}
				else if(attrValue.equals("Symptom")){
					symptom.add(nodeList.item(i).getFirstChild().getNodeValue().toString());
				}
				else if(attrValue.equals("Deficient")){
					nutDef.add(nodeList.item(i).getFirstChild().getNodeValue().toString());
				}
				else if(attrValue.equals("Excess")){
					nutEx.add(nodeList.item(i).getFirstChild().getNodeValue().toString());
				}
				else if(attrValue.equals("Need")){
					foodNe.add(nodeList.item(i).getFirstChild().getNodeValue().toString());
				}
				else if(attrValue.equals("Avoid")){
					foodAv.add(nodeList.item(i).getFirstChild().getNodeValue().toString());
				}				
			}
			
		}
		
		article.setConditionName(cond.get(0));
		article.setSynonyms(synonym);
		article.setSymptoms(symptom);
		article.setNeededFood(foodNe);
		article.setAvoidFood(foodAv);
		article.setNeededNutrients(nutDef);
		article.setAvoidNutrients(nutEx);
		
		return article;
	}

}
