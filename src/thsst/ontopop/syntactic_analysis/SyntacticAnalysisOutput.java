package thsst.ontopop.syntactic_analysis;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;

/**
 *Process the output of article retrieval module for lingpipe use.
 */
public class SyntacticAnalysisOutput {

    public static void main(String[] args) throws IOException {
        String filename;
        String line;
        String string;
        String URL;
        
        SyntacticAnalysisOutput synOut = new SyntacticAnalysisOutput(); 
        
        if (args.length == 0) {
            System.out.println("Enter filename for output processing.");
            System.out.print("?>");

            Scanner scanner = new Scanner(System.in);
            filename = scanner.nextLine();
            scanner.close();
        }
        else {
            filename = args[0];
			
        }
		
        ArrayList<String> paths = synOut.readFiles(new File(filename));
        for(int i=0; i<paths.size(); i++){
        	String file = paths.get(i);
		//Reads the contents of text file and stores it into a string.
	        BufferedReader reader = new BufferedReader(new FileReader(file));            
	        try {
	                      
				line = reader.readLine();
	            URL = line;
	            string = "";
	                      
	            while ((line = reader.readLine()) != null) { 
	            	string = string + "\n" + line;          
	            }
	            
	            System.out.println(string + "\n");
	            string.toString();
	                      
	        } finally {
	            reader.close();
						  
	        }
		
	        //Passes the read contents of text file for tagging.
	        //System.out.println(string.substring(211, 217));
        	
	        POSTagger tagger = new POSTagger();
	        ArrayList<ArrayList<TokenModel>> tokens = tagger.tag(string);
	        
	        String name = paths.get(i).substring(paths.get(i).lastIndexOf("\\")+1, paths.get(i).lastIndexOf("."));
	        
	        String fileName = new String("C:/Users/Paolo/Desktop/updated articles/WebMD/POS Output/"+name+".xml");
	        FileWriter writer = new FileWriter(fileName);
	        //String finalString = synOut.outputFormatter(tokens);
	        //System.out.println(finalString.substring(211, 217));
	        writer.write(synOut.outputFormatter(tokens, URL));
	        writer.close();
        }
        
		System.exit(0);
            
	}
    
    public ArrayList<String> readFiles(File folder){
    	ArrayList<String> paths = new ArrayList<String>();
    	if(folder.isDirectory()){
			for(File fileEntry: folder.listFiles()){
				if(fileEntry.isFile()){
					paths.add(fileEntry.getAbsolutePath());
				}
			}
		}
		else if(folder.isFile()){
			paths.add(folder.getAbsolutePath());
		}
    	
    	return paths;
    }
    
    public String outputFormatter(ArrayList<ArrayList<TokenModel>> tokens, String URL){
    	String output = "<article URL=\""+URL+"\"> ";
    	for(ArrayList<TokenModel> line: tokens){
	    	for(TokenModel token: line){
	    		output += "<"+token.getTag()+">"+token.getWord().toLowerCase()+"</"+token.getTag()+"> ";
	    	}
	    	//output+="<newLine>\\n</newLine>\n";
	    	output+="\n";
    	}
    	
    	output += "</article>";
    	return output;
    }
}

