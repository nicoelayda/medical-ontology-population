package thsst.ontopop;

import thsst.ontopop.core.view.CoreFrame;
import thsst.ontopop.retrieval.controller.ArticleRetrievalController;
import thsst.ontopop.retrieval.view.ArticleRetrievalView;

import javax.swing.*;

public class Main {

    public static void main(String[] args) throws Exception {
        // Set native operating system look and feel.
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

        // Program entry point here
        initGUI();
    }

    private static void initGUI() {
        //ArticleRetrievalView view = new ArticleRetrievalView();
        //ArticleRetrievalController controller = new ArticleRetrievalController(view);
    	CoreFrame cf = new CoreFrame();
    }
}
