package thsst.annotation;

import gate.AnnotationSet;
import gate.Corpus;
import gate.CorpusController;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.GateConstants;
import gate.LanguageAnalyser;
import gate.corpora.RepositioningInfo;
import gate.creole.SerialAnalyserController;
import gate.util.GateException;
import gate.util.Out;
import gate.util.persistence.PersistenceManager;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import org.apache.log4j.BasicConfigurator;

import thsst.ontopop.entity_recognition.TokenModel;
import thsst.ontopop.entity_recognition.EntityRecognizer.SortedAnnotationList;

public class Annotation {
	
	private CorpusController annieController;
	
	public void initAnnie() throws GateException, IOException {
	    Out.prln("Initialising ANNIE...");

	    // load the ANNIE application from the saved state in plugins/ANNIE
	    File pluginsHome = Gate.getPluginsHome();
	    File anniePlugin = new File(pluginsHome, "ANNIE");
	    File annieGapp = new File(anniePlugin, "ANNIE_with_defaults.gapp");
	    annieController = (CorpusController) PersistenceManager.loadObjectFromFile(annieGapp);

	    Out.prln("...ANNIE loaded");
	} // initAnnie()

	/** Tell ANNIE's controller about the corpus you want to run on */
	public void setCorpus(Corpus corpus) {
	    annieController.setCorpus(corpus);
	} // setCorpus

	/** Run ANNIE */
	public void execute() throws GateException {
	    Out.prln("Running ANNIE...");
	    annieController.execute();
	    Out.prln("...ANNIE complete");
	} // execute()

	/**
	 * Run from the command-line, with a list of URLs as argument.
	 * <P>
	 * <B>NOTE:</B><BR>
	 * This code will run with all the documents in memory - if you want to
	 * unload each from memory after use, add code to store the corpus in a
	 * DataStore.
	 */
	public void annotateXML(String src, String outputName) throws GateException, IOException {
	
	BasicConfigurator.configure(); 
	// initialise the GATE library
	Out.prln("Initialising GATE...");
	Gate.init();
	Out.prln("...GATE initialised");
	// load ANNIE plugin - you must do this before you can create tokeniser
	// or JAPE transducer resources.
	Gate.getCreoleRegister().registerDirectories(
	new File(Gate.getPluginsHome(), "ANNIE").toURI().toURL());
	
	//Annotation annie = new Annotation();
	//annie.initAnnie();

	 // Build the pipeline
	  SerialAnalyserController pipeline =
	 (SerialAnalyserController)Factory.createResource(
	   "gate.creole.SerialAnalyserController");
	  LanguageAnalyser tokeniser = (LanguageAnalyser)Factory.createResource(
	  "gate.creole.tokeniser.DefaultTokeniser");
	  
	LanguageAnalyser jape = (LanguageAnalyser)Factory.createResource(
	 "gate.creole.Transducer", gate.Utils.featureMap(
	     "grammarURL", new     
	 File("C:/Users/Paolo/Desktop/jape/symptom.jape").toURI().toURL(),
	   "encoding", "UTF-8")); // ensure this matches the file
	LanguageAnalyser jape1 = (LanguageAnalyser)Factory.createResource(
			 "gate.creole.Transducer", gate.Utils.featureMap(
			     "grammarURL", new     
	File("C:/Users/Paolo/Desktop/jape/newLine.jape").toURI().toURL(),
			   "encoding", "UTF-8")); // ensure this matches the file
	LanguageAnalyser jape2 = (LanguageAnalyser)Factory.createResource(
			 "gate.creole.Transducer", gate.Utils.featureMap(
			     "grammarURL", new     
	File("C:/Users/Paolo/Desktop/jape/symptom1.jape").toURI().toURL(),
			   "encoding", "UTF-8")); // ensure this matches the file
	LanguageAnalyser jape3 = (LanguageAnalyser)Factory.createResource(
			 "gate.creole.Transducer", gate.Utils.featureMap(
			     "grammarURL", new     
	File("C:/Users/Paolo/Desktop/jape/symptom2.jape").toURI().toURL(),
			   "encoding", "UTF-8")); // ensure this matches the file
	LanguageAnalyser jape4 = (LanguageAnalyser)Factory.createResource(
			 "gate.creole.Transducer", gate.Utils.featureMap(
			     "grammarURL", new     
	File("C:/Users/Paolo/Desktop/jape/symptom3.jape").toURI().toURL(),
			   "encoding", "UTF-8")); // ensure this matches the file
	LanguageAnalyser jape5 = (LanguageAnalyser)Factory.createResource(
			 "gate.creole.Transducer", gate.Utils.featureMap(
			     "grammarURL", new     
	File("C:/Users/Paolo/Desktop/jape/symptom4.jape").toURI().toURL(),
			   "encoding", "UTF-8")); // ensure this matches the file
	pipeline.add(tokeniser);
	pipeline.add(jape1);
	pipeline.add(jape);
	pipeline.add(jape2);
	pipeline.add(jape3);
	pipeline.add(jape4);
	pipeline.add(jape5);

	// create document and corpus
	// create a GATE corpus and add a document for each command-line
	// argument
	Corpus corpus = Factory.newCorpus("JAPE corpus");

	 URL u = new URL(src);
	 FeatureMap params = Factory.newFeatureMap();
	 params.put("sourceUrl", u);
	 params.put("preserveOriginalContent", new Boolean(true));
	 params.put("collectRepositioningInfo", new Boolean(true));
	 Out.prln("Creating doc for " + u);
	 Document doc = (Document)
	   Factory.createResource("gate.corpora.DocumentImpl", params);
	 corpus.add(doc);
	 pipeline.setCorpus(corpus);

	// run it
	pipeline.execute();

	// extract results
	/*System.out.println("Found annotations of the following types: " +
	  doc.getAnnotations().getAllTypes());*/

	Document doc1 = (Document) corpus.get(0);
    AnnotationSet defaultAnnotSet = doc1.getAnnotations();
    //defaultAnnotSet.get
    Set annotTypesRequired = new HashSet();
    annotTypesRequired.add("Condition");
    annotTypesRequired.add("Nutrient");
    annotTypesRequired.add("Food");
    annotTypesRequired.add("Symptom");
    
    //Print all annotation types
    for(String s: defaultAnnotSet.getAllTypes()){
  	  System.out.println(s);
    }
    
    //System.exit(0);
    
    Set<gate.Annotation> peopleAndPlaces = new HashSet<gate.Annotation>(defaultAnnotSet.get(annotTypesRequired));

    FeatureMap features = doc.getFeatures();
    String originalContent = (String)features.get(GateConstants.ORIGINAL_DOCUMENT_CONTENT_FEATURE_NAME);
    
    RepositioningInfo info = (RepositioningInfo)features.get(GateConstants.DOCUMENT_REPOSITIONING_INFO_FEATURE_NAME);
    // for each document, get an XML document with the
    // person and location names added
    
    Iterator it = peopleAndPlaces.iterator();
    gate.Annotation currAnnot;
    SortedAnnotationList sortedAnnotations = new SortedAnnotationList();

    while(it.hasNext()) {
      currAnnot = (gate.Annotation) it.next();
      sortedAnnotations.addSortedExclusive(currAnnot);
    } // while
    
    for(Object ann: sortedAnnotations){
    	gate.Annotation an = (gate.Annotation)ann;
    	//System.out.println(an.getType());
    }
    if(originalContent != null && info != null) {
    	ArrayList<ArrayList<TokenModel>> finalTokens = stringToTokenList(originalContent, sortedAnnotations, info);
    	/*for(TokenModel m: finalTokens){
    		System.out.println(m.getWord() + ", (" + m.getTag() + ")");
    	}*/
    	
    	System.out.println(finalTokens.size());
    	String finalString = "<article> ";
    	for(ArrayList<TokenModel> lineModels: finalTokens){
	    	for(TokenModel m: lineModels){
	    		finalString += m.toString()+" ";
	    	}
	    	finalString += "<newLine>\\n</newLine>\n";
    	}
    	finalString += "</article>";
    	
        String fileName = new String("C:/Users/Paolo/Desktop/Gold/Annot/Annot_"+outputName+".xml");
        FileWriter writer = new FileWriter(fileName);
        writer.write(finalString);
        writer.close();
    }
    
	}
	
	public static class SortedAnnotationList extends Vector {
	    public SortedAnnotationList() {
	      super();
	    } // SortedAnnotationList

	    public boolean addSortedExclusive(gate.Annotation annot) {
	      gate.Annotation currAnot = null;

	      // overlapping check
	      for (int i=0; i<size(); ++i) {
	        currAnot = (gate.Annotation) get(i);
	        if(annot.overlaps(currAnot)) {
	          return false;
	        } // if
	      } // for

	      long annotStart = annot.getStartNode().getOffset().longValue();
	      long currStart;
	      // insert
	      for (int i=0; i < size(); ++i) {
	        currAnot = (gate.Annotation) get(i);
	        currStart = currAnot.getStartNode().getOffset().longValue();
	        if(annotStart < currStart) {
	          insertElementAt(annot, i);
	          /*
	           Out.prln("Insert start: "+annotStart+" at position: "+i+" size="+size());
	           Out.prln("Current start: "+currStart);
	           */
	          return true;
	        } // if
	      } // for

	      int size = size();
	      insertElementAt(annot, size);
	//Out.prln("Insert start: "+annotStart+" at size position: "+size);
	      return true;
	    } // addSorted
	  } // SortedAnnotationList
	
	private ArrayList<ArrayList<TokenModel>> stringToTokenList(String originalContent, SortedAnnotationList sortedAnnotations, RepositioningInfo info){
		  ArrayList<ArrayList<TokenModel>> tokenList = new  ArrayList<ArrayList<TokenModel>>();
		  //originalContent = originalContent.substring(7, originalContent.length()-7);
		  
		  //System.out.println(originalContent);
		  
		  String body = (originalContent.replace("<article>", ""));
		  body = (body.replace("</article>", "")).trim();
		  String[] lines = body.split("\n");	
		  //String[] tokens = originalContent.split("\\s+");	
		  
		  ArrayList<ArrayList<String>> newTokens = new ArrayList<ArrayList<String>>();
		  for(int i=0; i<lines.length; i++){
			  if(lines[i].trim().length() > 0){
				  ArrayList<String> lineTokens = new ArrayList<String>();
				  String[] tokens = lines[i].trim().split(" <");
				  for(int y=0; y<tokens.length; y++){
					  lineTokens.add(tokens[y].trim());
				  }
				  newTokens.add(lineTokens);
			  }
		  }

		  long currStart = 1;
		  
		  TokenModel tokenModel;
		  int qwe = 0;
		  for(ArrayList<String> lineTokens: newTokens){
			  //System.out.println(qwe + " : " + newTokens.size());
			  ArrayList<TokenModel> lineModels = new ArrayList<TokenModel>();
			  for(String token: lineTokens){
				  if(token.charAt(0) != '<'){
					  token = "<"+token;
				  }
				  tokenModel = new TokenModel();
				  //System.out.println((int)currStart+1 +", "+ token.indexOf(">"));
				  //System.out.println(token);
				  String tag = token.substring(1, token.indexOf(">"));
				  tokenModel.setTag(tag);
				  tokenModel.setStart(currStart);
				  String word = token.substring(token.indexOf(">")+1, token.lastIndexOf("<"));
				  tokenModel.setWord(word);
				  tokenModel.setEnd(tokenModel.getStart()+word.length());
				  
				  currStart = tokenModel.getEnd()+1;
				  
				  lineModels.add(tokenModel);
			  }
			  tokenList.add(lineModels);
			  currStart += 1;
			  qwe++;
		  }
		  
		  return addNewTags(tokenList, sortedAnnotations, info, originalContent);
	  } 
	  
	  private ArrayList<ArrayList<TokenModel>> adjustPosition(ArrayList<ArrayList<TokenModel>> tokens){
		  //System.out.println("ADJUSTING!!!!!!!!!!!!!!!!!");
		  
		  ArrayList<ArrayList<TokenModel>> newTokens = new ArrayList<ArrayList<TokenModel>>();
		  ArrayList<TokenModel> newList;
		  
		  int currCount = 1;
		  
		  for(ArrayList<TokenModel> tokenList : tokens){
			  newList = new ArrayList<TokenModel>();
			  for(TokenModel token: tokenList){
				  token.setStart(currCount);
				  currCount += token.getWord().length()-1;
				  token.setEnd(currCount);
				  
				  currCount += 2;
				  
				  newList.add(token);
			  }
			  newTokens.add(newList);
		  }
		  //System.out.println(tokenList.get(0).getStart() + "WHAAAT!!!!!!!!!!!!!!!!11");
		  
		  for(ArrayList<TokenModel> listM: newTokens){
			  for(TokenModel m: listM){
				  System.out.println(m.getWord());
				  System.out.println(m.getStart()+" : "+m.getEnd());
			  }
		  }
		  
		  //System.exit(0);
		  
		  return newTokens;
	  } 
	  
	  private ArrayList<ArrayList<TokenModel>> addNewTags(ArrayList<ArrayList<TokenModel>> tokens, SortedAnnotationList sortedAnnotations, RepositioningInfo info, String originalContent){
		  gate.Annotation currAnnot;
		  TokenModel currToken;
		  long insertPositionStart;
	      long insertPositionEnd;
		  
		  int y = 0;
		  
		  boolean adjusted = false;
		  boolean found = false;
		  
		  for(int i=0; i<sortedAnnotations.size(); i++){
			  currAnnot = (gate.Annotation)sortedAnnotations.get(i);
			  found = false;
			  //System.out.println(currAnnot.getType());
			  for(; y<tokens.size(); y++){
				  ArrayList<TokenModel> lineModels = tokens.get(y);
				  for(int j=0; j<lineModels.size(); j++){
					  currToken = lineModels.get(j);
					  insertPositionStart = currAnnot.getStartNode().getOffset().longValue();
					  insertPositionEnd = currAnnot.getEndNode().getOffset().longValue();
					  
					  //System.out.println("pre: "+insertPositionStart +" "+ insertPositionEnd);
					  //System.out.println("token: "+currToken.getStart() +" "+ currToken.getEnd());
					  
			          insertPositionStart = info.getOriginalPos(insertPositionStart);
			          insertPositionEnd = info.getOriginalPos(insertPositionEnd, true);
			          
			          //System.out.println(insertPositionStart +" "+ insertPositionEnd);
			          if(insertPositionEnd != -1 && insertPositionStart != -1){
			        	  //System.out.println("Exist!");
			        	  insertPositionStart = currAnnot.getStartNode().getOffset().longValue();
						  insertPositionEnd = currAnnot.getEndNode().getOffset().longValue();
			          }
			          else{
			        	  //System.out.println("Does not Exist");
			        	  if(!adjusted){
			        		  tokens = adjustPosition(tokens);
			        		  currToken = tokens.get(y).get(j);
			        		  adjusted = !adjusted;
			        	  }
			        	  insertPositionStart = currAnnot.getStartNode().getOffset().longValue();
						  insertPositionEnd = currAnnot.getEndNode().getOffset().longValue();
						  //System.out.println(insertPositionStart +" : "+ insertPositionEnd);
			          }
			          
			          System.out.println(currAnnot.getType()+" : "+insertPositionStart +" : "+ insertPositionEnd);
			          System.out.println(currToken.getStart() +" : "+ currToken.getEnd());
			          System.out.println(currToken.toString());
			          System.out.println();
			          //System.out.println(currToken.getStart());
			          //System.out.println(currAnnot.getType());
			          //System.out.println(currToken.toString());
			        
			          if(insertPositionStart == currToken.getStart()){
						  TokenModel tokenToTag = currToken;
						  lineModels.remove(j);
						  while(insertPositionEnd > currToken.getEnd()){
							  tokenToTag = mergeTokens(tokenToTag, lineModels.get(j));
							  currToken = lineModels.get(j);
							  lineModels.remove(j);
						  }
						  tokenToTag.setTag("Entity");
						  tokenToTag.getAttributes().put("Type", currAnnot.getType());
						  lineModels.add(j, tokenToTag);
						  found = true;
						  break;
					  }
			         
				  }
				  if(found){
		        	  break;
		          }
			  }
		  }
		  
		  return tokens;
	  }
	  
	  private TokenModel mergeTokens(TokenModel currToken, TokenModel nextToken){
		  TokenModel mergedToken = new TokenModel();
		  
		  mergedToken.setWord(currToken.getWord() + " " + nextToken.getWord());
		  mergedToken.setStart(currToken.getStart());
		  mergedToken.setEnd(nextToken.getEnd());
		  mergedToken.setTag("");
		  
		  return mergedToken;
	  }
	
}



