import thsst.ontopop.retrieval.Cleaner;

import java.io.File;

public class CleaningTest {

    private static final String STORAGE_PATH = "tmp/";

    public static void main(String[] args) {
        Cleaner cleaner = new Cleaner(STORAGE_PATH);
        File[] fileList = new File(STORAGE_PATH + "/raw").listFiles();

        for (File file : fileList) {
            System.out.println("Cleaning: " + file.getName());
            cleaner.clean(file);
        }

        System.out.println("Cleaning is finished!");
    }
}
